//
//  Notify.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/11.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import Foundation

struct Notify {
    var datetime:String
    var content:String
    var isRead:Bool
}
