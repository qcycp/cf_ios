//
//  Case.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/13.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import Foundation

struct Case {
    var date: String
    var ownerId: String
    var ownerName: String
    var caseId: String
    var status: String
}
