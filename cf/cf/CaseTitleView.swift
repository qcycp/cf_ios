//
//  CaseTitleView.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/13.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class CaseTitleView: UIView {

    private lazy var titleView: UIView = {
        let titleFrame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kCaseTableViewTitleHeight)
        let titleView = UIView(frame: titleFrame)
        titleView.backgroundColor = .white
        
        let labelW: CGFloat = kScreenWidth / 4
        let labelH: CGFloat = kCaseTableViewTitleHeight
        let labelY: CGFloat = 0

        let dateLabel = UILabel()
        dateLabel.text = "進件日期"
        dateLabel.font = UIFont.systemFont(ofSize: 16.0)
        dateLabel.textColor = UIColor.darkGray
        dateLabel.textAlignment = .center
        dateLabel.frame = CGRect(x: 0 * labelW, y: labelY, width: labelW, height: labelH)
        titleView.addSubview(dateLabel)
        
        let ownerLabel = UILabel()
        ownerLabel.text = "申請人"
        ownerLabel.font = UIFont.systemFont(ofSize: 16.0)
        ownerLabel.textColor = UIColor.darkGray
        ownerLabel.textAlignment = .center
        ownerLabel.frame = CGRect(x: 1 * labelW, y: labelY, width: labelW, height: labelH)
        titleView.addSubview(ownerLabel)
        
        let caseIdLabel = UILabel()
        caseIdLabel.text = "案件來源"
        caseIdLabel.font = UIFont.systemFont(ofSize: 16.0)
        caseIdLabel.textColor = UIColor.darkGray
        caseIdLabel.textAlignment = .center
        caseIdLabel.frame = CGRect(x: 2 * labelW, y: labelY, width: labelW, height: labelH)
        titleView.addSubview(caseIdLabel)
        
        let statusLabel = UILabel()
        statusLabel.text = "狀態"
        statusLabel.font = UIFont.systemFont(ofSize: 16.0)
        statusLabel.textColor = UIColor.darkGray
        statusLabel.textAlignment = .center
        statusLabel.frame = CGRect(x: 3 * labelW, y: labelY, width: labelW, height: labelH)
        titleView.addSubview(statusLabel)

        return titleView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(titleView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
