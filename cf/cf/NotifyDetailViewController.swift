//
//  NotifyDetailViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/11.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class NotifyDetailViewController: UIViewController {

    @IBOutlet weak var datetime: UILabel!
    @IBOutlet weak var content: UITextView!
    var notify: Notify?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datetime.text = notify?.datetime
        content.text = notify?.content
    }
}
