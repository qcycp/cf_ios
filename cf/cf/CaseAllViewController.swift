//
//  CaseAllViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/13.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

private let TableCellID = "TableCellID"

class CaseAllViewController: UIViewController {
    
    private var data: [Case] = {
        let data = [
            Case(date: "20191006", ownerId: "1111", ownerName: "王小明", caseId: "FT07705018", status: "審核中"),
            Case(date: "20191005", ownerId: "2222", ownerName: "王小明", caseId: "FT07705017", status: "審核中"),
            Case(date: "20191004", ownerId: "3333", ownerName: "王小明", caseId: "FT07705016", status: "審核中"),
            Case(date: "20191003", ownerId: "4444", ownerName: "王小明", caseId: "FT07705015", status: "核准"),
            Case(date: "20191002", ownerId: "5555", ownerName: "王小明", caseId: "FT07705014", status: "核准"),
            Case(date: "20191001", ownerId: "6666", ownerName: "王小明", caseId: "FT07705013", status: "核准"),
            Case(date: "20190930", ownerId: "7777", ownerName: "王小明", caseId: "FT07705012", status: "婉拒"),
            Case(date: "20190929", ownerId: "8888", ownerName: "王小明", caseId: "FT07705011", status: "婉拒"),
            Case(date: "20190928", ownerId: "9999", ownerName: "王小明", caseId: "FT07705010", status: "婉拒"),
            Case(date: "20190927", ownerId: "0000", ownerName: "王小明", caseId: "FT07705009", status: "婉拒"),
            Case(date: "20190926", ownerId: "1234", ownerName: "王小明", caseId: "FT07705008", status: "婉拒")]
        return data
    }()
    
    private lazy var caseTitleView: CaseTitleView = {
        let titleFrame: CGRect = CGRect(x: 0, y: 0, width: kScreenWidth, height: kCaseTableViewTitleHeight)
        let caseTitleView: CaseTitleView = CaseTitleView(frame: titleFrame)
        return caseTitleView
    }()
    
    private lazy var tableView: UITableView = {
        let dataFrame = CGRect(x: 0, y: kCaseTableViewTitleHeight, width: kScreenWidth, height: kScreenHeight - kStatusBarHeight - kNavigationBarHeight - kPageTitleViewHeight - kCaseTableViewTitleHeight - kTabBarHeight)
        let tableView = UITableView(frame: dataFrame)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.register(DataTableViewCell.self, forCellReuseIdentifier: TableCellID)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(caseTitleView)
        view.addSubview(tableView)
    }
}

extension CaseAllViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellID, for: indexPath) as! DataTableViewCell
            cell.data = data[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = CaseDetailViewController()
        destination.hidesBottomBarWhenPushed = true
        destination.data = data[indexPath.row]
        navigationController?.pushViewController(destination, animated: true)
        
    }
}

extension CaseAllViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kCaseTableViewRowHeight
    }
}
