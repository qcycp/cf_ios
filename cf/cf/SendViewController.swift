//
//  SendViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/12.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class SendViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "送件"
        self.navigationController?.navigationBar.barTintColor = .orange
        self.navigationController?.navigationBar.isTranslucent = false
    }
}
