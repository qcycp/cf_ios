//
//  AppDelegate.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/10.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow.init()
        window?.frame = UIScreen.main.bounds
        window?.makeKeyAndVisible()
        window?.rootViewController = UIStoryboard.init(name: "Login", bundle: nil).instantiateInitialViewController()
        
        //set the color of tar bar image when selected
        UITabBar.appearance().tintColor = UIColor.orange
        return true
    }
}

