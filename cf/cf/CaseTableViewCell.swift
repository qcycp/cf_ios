//
//  CaseTableViewCell.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/13.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class DataTableViewCell: UITableViewCell {

    var data : Case? {
        didSet {
            dateLabel.text = data?.date
            ownerIdLabel.text = data?.ownerId
            ownerNameLabel.text = data?.ownerName
            caseIdLabel.text = data?.caseId
            statusLabel.text = data?.status
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    private let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemWidth = kScreenWidth / 4
        label.frame = CGRect(x: 0 * itemWidth, y: 0, width: itemWidth, height: kCaseTableViewRowHeight)
        return label
    }()
    
    private let ownerIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemWidth = kScreenWidth / 4
        label.frame = CGRect(x: 1 * itemWidth, y: 0, width: itemWidth, height: kCaseTableViewRowHeight/2)
        return label
    }()
    
    private let ownerNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemWidth = kScreenWidth / 4
        label.frame = CGRect(x: 1 * itemWidth, y: kCaseTableViewRowHeight/2, width: itemWidth, height: kCaseTableViewRowHeight/2)
        return label
    }()
    
    private let caseIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemWidth = kScreenWidth / 4
        label.frame = CGRect(x: 2 * itemWidth, y: 0, width: itemWidth, height: kCaseTableViewRowHeight)
        return label
    }()
    
    private let statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemWidth = kScreenWidth / 4
        label.frame = CGRect(x: 3 * itemWidth, y: 0, width: itemWidth, height: kCaseTableViewRowHeight)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(dateLabel)
        addSubview(ownerIdLabel)
        addSubview(ownerNameLabel)
        addSubview(caseIdLabel)
        addSubview(statusLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
