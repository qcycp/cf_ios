//
//  NotifyTableViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/11.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class NotifyTableViewController: UITableViewController {

    var items = [Notify(datetime: "2019/10/06 19:25", content: "[系統公告] test", isRead: false), Notify(datetime: "2019/10/06 18:25", content: "[案件回覆] test", isRead: false), Notify(datetime: "2019/10/06 17:25", content: "[業務訊息] test", isRead: false), Notify(datetime: "2019/10/06 16:25", content: "[補件通知] test", isRead: false), Notify(datetime: "2019/10/06 15:25", content: "[系統公告] test", isRead: false), Notify(datetime: "2019/10/06 14:25", content: "[案件回覆] test", isRead: false), Notify(datetime: "2019/10/06 13:25", content: "[業務訊息] test", isRead: false), Notify(datetime: "2019/10/06 12:25", content: "[補件通知] test", isRead: false), Notify(datetime: "2019/10/06 11:25", content: "[系統公告] test", isRead: false), Notify(datetime: "2019/10/06 10:25", content: "[案件回覆] test", isRead: false), Notify(datetime: "2019/10/06 9:25", content: "[業務訊息] test", isRead: false), Notify(datetime: "2019/10/06 8:25", content: "[補件通知] 貴公司/商工行號申請商品驗證登錄(受理編號: 12345678 )，所應檢附之文件資料因部分謬誤或疏漏，請於本案通知日起二個月內速予補正。逾期不補正者，將判定為驗證不符，並予結案。\n[補件通知] 貴公司/商工行號申請商品驗證登錄(受理編號: 12345678 )，所應檢附之文件資料因部分謬誤或疏漏，請於本案通知日起二個月內速予補正。逾期不補正者，將判定為驗證不符，並予結案。\n[補件通知] 貴公司/商工行號申請商品驗證登錄(受理編號: 12345678 )，所應檢附之文件資料因部分謬誤或疏漏，請於本案通知日起二個月內速予補正。逾期不補正者，將判定為驗證不符，並予結案。", isRead: false)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "通知"
        self.navigationController?.navigationBar.barTintColor = .orange
        self.navigationController?.navigationBar.isTranslucent = false
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notifyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "notifyTableViewCell", for: indexPath) as! NotifyTableViewCell
        notifyTableViewCell.accessoryType = .disclosureIndicator
        notifyTableViewCell.datetime.text = items[indexPath.row].datetime
        notifyTableViewCell.content.text = items[indexPath.row].content
        return notifyTableViewCell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let row = tableView.indexPathForSelectedRow?.row
        let notifyDetailVC = segue.destination as! NotifyDetailViewController
        notifyDetailVC.notify = items[row!]
        notifyDetailVC.hidesBottomBarWhenPushed = true
    }
}
