//
//  LoginViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/11.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var error: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        error.isHidden = true
        username.placeholder = "使用者帳號"
        password.placeholder = "使用者密碼"
    }

    @IBAction func username_action(_ sender: Any) {
        error.isHidden = true
    }
    
    @IBAction func password_action(_ sender: Any) {
        error.isHidden = true
    }
    
    @IBAction func forget_password(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "請聯繫業務：（03) 260-0000", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func submit(_ sender: Any) {
        let MainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let MainViewController = MainStoryboard.instantiateViewController(withIdentifier: "Main")
        MainViewController.modalPresentationStyle = .fullScreen
        MainViewController.modalTransitionStyle = .crossDissolve
        self.present(MainViewController, animated: true, completion: nil)
        
        self.view.endEditing(true)
        if let _username = username.text {
            if _username.isEmpty {
                error.text = "請輸入使用者帳號"
                error.isHidden = false
                return
            }
        }
        if let _password = password.text {
            if _password.isEmpty {
                error.text = "請輸入使用者密碼"
                error.isHidden = false
                return
            }
        }
        
        if username.text != "test" || password.text != "1234" {
            error.text = "帳號或密碼輸入錯誤"
            error.isHidden = false
        } else {
            error.isHidden = true
            
            //goto Main
            let MainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let MainViewController = MainStoryboard.instantiateViewController(withIdentifier: "Main")
            MainViewController.modalPresentationStyle = .fullScreen
            MainViewController.modalTransitionStyle = .crossDissolve
            self.present(MainViewController, animated: true, completion: nil)
        }
    }
}
