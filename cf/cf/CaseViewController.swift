//
//  CaseViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/12.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class CaseViewController: UIViewController {

    private lazy var pageTitleView: PageTitleView = {[weak self] in
        let titleFrame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kPageTitleViewHeight)
        let titles = ["全部", "核准", "審核中", "婉拒"]
        let titleView = PageTitleView(frame: titleFrame, titles: titles)
        titleView.delegate = self
        return titleView
    }()
    
    private lazy var pageContentView: PageContentView = {[weak self] in
        let contentFrame = CGRect(x: 0, y: kPageTitleViewHeight, width: kScreenWidth, height: kScreenHeight - kStatusBarHeight - kNavigationBarHeight - kPageTitleViewHeight)
        var childVcs = [UIViewController]()
        childVcs.append(CaseAllViewController())
        childVcs.append(CaseApproveViewController())
        childVcs.append(CaseReviewingViewController())
        childVcs.append(CaseDenyViewController())
        let contentView = PageContentView(frame: contentFrame, childVcs: childVcs, parentViewController: self)
        contentView.delegate = self
        return contentView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "案件查詢"
        self.navigationController?.navigationBar.barTintColor = .orange
        self.navigationController?.navigationBar.isTranslucent = false

        view.addSubview(pageTitleView)
        view.addSubview(pageContentView)
    }
}

//after clicking PageTitle, do something via delegate
extension CaseViewController: PageTitleViewDelegate {
    func pageTitleView(titleView: PageTitleView, selectedIndex index: Int) {
        pageContentView.setCurrentIndex(currentIndex: index)
    }
}

//after dragging PageContent, do something via delegate
extension CaseViewController: PageContentViewDelegate {
    func pageContentView(contentView: PageContentView, progress: CGFloat, sourceIndex: Int, targetIndex: Int) {
        pageTitleView.setTitleWithProgress(progress: progress, sourceIndex: sourceIndex, targetIndex: targetIndex)
    }
}
