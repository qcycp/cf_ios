//
//  Common.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/12.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

//general
let kStatusBarHeight: CGFloat = 20
let kNavigationBarHeight: CGFloat = 44
let kTabBarHeight: CGFloat = 49

let kScreenWidth = UIScreen.main.bounds.width
let kScreenHeight = UIScreen.main.bounds.height

//case page
let kPageTitleViewHeight: CGFloat = 40
let kPageTitleViewLineHeight: CGFloat = 2
let kCaseTableViewTitleHeight: CGFloat = 40
let kCaseTableViewRowHeight: CGFloat = 60
