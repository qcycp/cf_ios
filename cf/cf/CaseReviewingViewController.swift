//
//  CaseReviewingViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/13.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

private let TableCellID = "TableCellID"

class CaseReviewingViewController: UIViewController {
    
    private var data: [Case] = {
        let data = [
            Case(date: "20191006", ownerId: "1111", ownerName: "王小明", caseId: "FT07705018", status: "審核中"),
            Case(date: "20191005", ownerId: "2222", ownerName: "王小明", caseId: "FT07705017", status: "審核中"),
            Case(date: "20191004", ownerId: "3333", ownerName: "王小明", caseId: "FT07705016", status: "審核中")]
        return data
    }()
    
    private lazy var caseTitleView: CaseTitleView = {
        let titleFrame: CGRect = CGRect(x: 0, y: 0, width: kScreenWidth, height: kCaseTableViewTitleHeight)
        let caseTitleView: CaseTitleView = CaseTitleView(frame: titleFrame)
        return caseTitleView
    }()
    
    private lazy var tableView: UITableView = {
        let dataFrame = CGRect(x: 0, y: kCaseTableViewTitleHeight, width: kScreenWidth, height: kScreenHeight - kStatusBarHeight - kNavigationBarHeight - kPageTitleViewHeight - kCaseTableViewTitleHeight - kTabBarHeight)
        let tableView = UITableView(frame: dataFrame)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.register(DataTableViewCell.self, forCellReuseIdentifier: TableCellID)
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(caseTitleView)
        view.addSubview(tableView)
    }
}

extension CaseReviewingViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableCellID, for: indexPath) as! DataTableViewCell
            cell.data = data[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = CaseDetailViewController()
        destination.hidesBottomBarWhenPushed = true
        destination.data = data[indexPath.row]
        navigationController?.pushViewController(destination, animated: true)
        
    }
}

extension CaseReviewingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kCaseTableViewRowHeight
    }
}

