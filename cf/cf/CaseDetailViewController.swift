//
//  CaseDetailViewController.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/13.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

private let CaseDetailID = "CaseDetail"

class CaseDetailViewController: UIViewController {

    var data: Case?
    private lazy var scrollView: UIScrollView = {
        let scrollFrame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        let scrollView = UIScrollView(frame: scrollFrame)
        scrollView.showsVerticalScrollIndicator = true
        scrollView.isScrollEnabled = true
        scrollView.scrollsToTop = false
        scrollView.bounces = true
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemHeight = kScreenHeight / 5
        label.frame = CGRect(x: 0, y: 0 * itemHeight, width: kScreenWidth, height: itemHeight)
        label.backgroundColor = .red
        return label
    }()
    
    private let ownerIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemHeight = kScreenHeight / 5
        label.frame = CGRect(x: 0, y: 1 * itemHeight, width: kScreenWidth, height: itemHeight)
        label.backgroundColor = .cyan
        return label
    }()
    
    private let ownerNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemHeight = kScreenHeight / 5
        label.frame = CGRect(x: 0, y: 2 * itemHeight, width: kScreenWidth, height: itemHeight)
        label.backgroundColor = .green
        return label
    }()
    
    private let caseIdLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemHeight = kScreenHeight / 5
        label.frame = CGRect(x: 0, y: 3 * itemHeight, width: kScreenWidth, height: itemHeight)
        label.backgroundColor = .blue
        return label
    }()
    
    private let statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        
        let itemHeight = kScreenHeight / 5
        label.frame = CGRect(x: 0, y: 4 * itemHeight, width: kScreenWidth, height: itemHeight)
        label.backgroundColor = .purple
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(scrollView)
        scrollView.addSubview(dateLabel)
        scrollView.addSubview(ownerIdLabel)
        scrollView.addSubview(ownerNameLabel)
        scrollView.addSubview(caseIdLabel)
        scrollView.addSubview(statusLabel)
        
        // constrain the scroll view to 0-pts on each side
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0.0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true

        // constrain the first subview to top with 0-pts padding
        dateLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0.0).isActive = true
        // constrain last subview to bottom with 15-pts padding
        statusLabel.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0.0).isActive = true

        dateLabel.text = data?.date
        ownerIdLabel.text = data?.ownerId
        ownerNameLabel.text = data?.ownerName
        caseIdLabel.text = data?.caseId
        statusLabel.text = data?.status
    }
}
