//
//  NotifyTableViewCell.swift
//  cf
//
//  Created by YU-LIANG YEH on 2019/10/11.
//  Copyright © 2019 YU-LIANG YEH. All rights reserved.
//

import UIKit

class NotifyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var datetime: UILabel!
    @IBOutlet weak var content: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
